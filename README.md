# Borneo Testing Exercise

## Problem Statement
Problem statement: Formulate & implement a basic testing plan for the Inspection REST API
that’s part of Google’s Data Loss Prevention (DLP) solution.

Deliverables:
```

1. Write a test plan for the above problem.
2. Implement the test suite as described above. You can use github to share the test plan
and solution. Make sure you commit your code at regular intervals, don’t push all the
code as a single commit.
3. Running setup to demo during the interview.
```


## Google DLP Inspection API example
Using curl, you can invoke the REST API like this:
```buildoutcfg
curl
    'https://dlp.googleapis.com/v2/projects/qe-assignment/content:inspect
    ?key=[API key]' \
    --header 'Content-Type: application/json' \
    --data '{"item":{"value":"My credit card is 5555555555554444"}}'
```
### A sample result looks like this:

```buildoutcfg
{
  "result": {
    "findings": [
      {
        "infoType": {
          "name": "CREDIT_CARD_NUMBER"
        },
        "likelihood": "LIKELY",
        "location": {
          "byteRange": {
            "start": "18",
            "end": "34"
          },
          "codepointRange": {
            "start": "18",
            "end": "34"
          }
        },
        "createTime": "2021-06-02T16:58:24.549Z",
        "findingId": "2021-06-02T16:58:24.556431Z8852925743517645955"
      }
    ]
  }
}
```

## Scope
The Scope of the Test Plan is as mentioned below:
```buildoutcfg
=> Test Plan and automated test scenarios only covers the Text data type
=> Test Data Generator to generate the dynamic input text
=> Functional test scenarios
```
## Out of Scope
Below is list not in the scope of this test plan:
```buildoutcfg
=> DLP API for Image processing
=> No other type of testing like Performance, Security etc.
```

## Dependencies
### Set the PYTHONPATH
```buildoutcfg
export PYTHONPATH=$HOME/borneo/lib
```
### Install, create and activate the virtual env
```buildoutcfg
pip3 install virtualenv
virtualenv --python=python3 api-test
source api-test/bin/activate
```

### Install the poetry and install the dependencies
```buildoutcfg
curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python3
source /home/ubuntu/.poetry/env
poetry update
```

### Add API key to access the Google DLP API to an environment variable
```buildoutcfg
export API_KEY=<API KEY>
```

### Command to run the tests
```buildoutcfg
python3 -m pytest -v --html=reports/report.html --self-contained-html tests/api/test_inspect.py
```

### To check the logs, open html file
```buildoutcfg
open reports/report.html
```