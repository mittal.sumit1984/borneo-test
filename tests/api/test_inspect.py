import pytest
import logging
import os

from lib.Borneo.Inspector.Inspector import Inspector
from lib.Borneo.Validator import Validator
from lib.utils.data_generator import generate_sample_payloads

LOGGER = logging.getLogger(__name__)

input_data_set = [
    ("183.187.109.256", "PHONE_NUMBER", ["UNLIKELY", "POSSIBLE"], None, 15),
    ("183.187.109.255", "IP_ADDRESS", ["POSSIBLE", "LIKELY"], None, 15),
    ("abc.187.109.255", "PHONE_NUMBER", ["UNLIKELY"], 4, 15),
    ("4Y1SL65848Z411439", "US_VEHICLE_IDENTIFICATION_NUMBER", ["POSSIBLE"], None, 17),
]


@pytest.mark.parametrize(
    "content,expected_label,likelihood,start,end",
    input_data_set)
def test_inspect_custom_data(content, expected_label, likelihood, start, end):
    obj = Inspector(
        project_id="qe-assignment",
        api_key=os.environ['API_KEY']
    )

    LOGGER.info("CONTENT: " + content)
    response = obj.inspect(content)
    expected_data = {
        "label": expected_label,
        "start": start,
        "end": end,
        "likelihood": likelihood
    }

    v_obj = Validator(response)
    assert v_obj.validate(expected_data)


def test_inspect_with_empty_data():
    obj = Inspector(
        project_id="qe-assignment",
        api_key=os.environ['API_KEY']
    )

    content = ""
    response = obj.inspect(content)
    LOGGER.info(response.json())

    inspect_rsp = response.json()
    assert inspect_rsp["error"]["code"] == 400
    assert inspect_rsp["error"]["status"] == "INVALID_ARGUMENT"
    assert "ContentItem has no data to inspect" in inspect_rsp["error"]["message"]


input_data_set = [
    (["email"], True, 2, "EMAIL_ADDRESS", ["LIKELY", "POSSIBLE"]),
    (["ssn"], True, 2, "US_SOCIAL_SECURITY_NUMBER", ["POSSIBLE", "LIKELY", "UNLIKELY"]),
    (["creditcard"], True, 2, "CREDIT_CARD_NUMBER", ["POSSIBLE", "LIKELY", "UNLIKELY"]),
    (["phone-numbers"], True, 3, "PHONE_NUMBER", ["POSSIBLE", "LIKELY", "UNLIKELY"]),
    (["ip-address"], True, 3, "IP_ADDRESS", ["POSSIBLE", "LIKELY", "UNLIKELY"]),
]


@pytest.mark.parametrize(
    "labels,valid_data,dataset_per_label,expected_label,likelihood",
    input_data_set)
def test_inspect_valid_data(
        labels,
        valid_data,
        dataset_per_label,
        expected_label,
        likelihood):
    data_set = generate_sample_payloads(labels, valid_data, dataset_per_label)

    obj = Inspector(
        project_id="qe-assignment",
        api_key=os.environ['API_KEY']
    )

    for content in data_set:
        LOGGER.info("CONTENT: " + content)
        response = obj.inspect(content)
        expected_data = {
            "label": expected_label,
            "likelihood": likelihood
        }

        v_obj = Validator(response)
        assert v_obj.validate_dynamic_data(expected_data)


input_data_set = [
    (pytest.param(
        ["email"],
        False,
        2,
        ["EMAIL_ADDRESS"],
        "",
        marks=pytest.mark.xfail(
            reason='expected failure')))]


@pytest.mark.parametrize(
    "labels,valid_data,dataset_per_label,expected_label,likelihood",
    input_data_set,
)
def test_inspect_invalid_data(
        labels,
        valid_data,
        dataset_per_label,
        expected_label,
        likelihood):
    data_set = generate_sample_payloads(labels, valid_data, dataset_per_label)

    obj = Inspector(
        project_id="qe-assignment",
        api_key=os.environ['API_KEY']
    )

    for content in data_set:
        LOGGER.info("CONTENT: " + content)
        response = obj.inspect(content)
        expected_data = {
            "label": expected_label,
            "start": 16,
            "end": (len(content) - 16),
            "likelihood": likelihood
        }

        v_obj = Validator(response)
        assert v_obj.validate(expected_data)


# True Negative Tests
data_set = [
    "sumit@gmailcom",
    "sumit@@gmail.com",
    "sumit@gmail.c&m",
    "IP is 0.0.0.256",
    "1.2.03.01",
    "1.-1.1.1"]


@pytest.mark.parametrize("content", data_set)
def test_inspect_invalid_content(content):
    obj = Inspector(
        project_id="qe-assignment",
        api_key=os.environ['API_KEY']
    )

    LOGGER.info("CONTENT: " + content)
    response = obj.inspect(content)

    inspect_rsp = response.json()
    LOGGER.info(inspect_rsp["result"])

    assert not inspect_rsp["result"]
