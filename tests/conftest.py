import pytest
import logging

from lib.utils.data_generator import generate_sample_payloads

LOGGER = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def generate_data_set():
    def pass_params(labels, valid_data=True, dataset_per_label=1):
        payload = generate_sample_payloads(labels, valid_data, dataset_per_label)
        LOGGER.info(payload)
        return payload

    return []
