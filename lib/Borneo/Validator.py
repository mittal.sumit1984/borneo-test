import logging

LOGGER = logging.getLogger(__name__)


class Validator:
    def __init__(self, response):
        """
        :param response: this is the response of DLP API
        """
        if response.status_code in [200, 201, 202]:
            self.response = response.json()
            LOGGER.info(self.response)
        else:
            raise "API response is non 2xx"

    def validate(self, expected_result):
        """
        :param expected_result: hash of expected values to be checked
        :return:
        """

        result = True
        if not self.response:
            return False

        if "findings" not in self.response["result"]:
            return False

        for block in self.response["result"]["findings"]:
            if "start" in block["location"]["byteRange"]:
                if block["location"]["byteRange"]["start"] != str(expected_result["start"]):
                    LOGGER.info(expected_result["start"])
                    LOGGER.info(block["location"]["byteRange"]["start"])
                    result = False

            if "end" in block["location"]["byteRange"]:
                if block["location"]["byteRange"]["end"] != str(expected_result["end"]):
                    LOGGER.info(expected_result["end"])
                    LOGGER.info(block["location"]["byteRange"]["end"])
                    result = False

            if expected_result["label"] != block["infoType"]["name"]:
                result = False

            is_label = False
            for likelihood in expected_result["likelihood"]:
                if likelihood == block["likelihood"]:
                    is_label = True
            if not is_label:
                result = False

        return result

    def validate_dynamic_data(self, expected_result):
        """
        :param expected_result: hash of expected values to be checked
        :return:
        """

        result = True
        if not self.response:
            return False

        if "findings" not in self.response["result"]:
            return False

        is_label = False
        for block in self.response["result"]["findings"]:
            if expected_result["label"] == block["infoType"]["name"]:
                is_label = True
                break
        if not is_label:
            result = False

        is_likelihood = False
        for block in self.response["result"]["findings"]:
            for likelihood in expected_result["likelihood"]:
                if likelihood == block["likelihood"]:
                    is_likelihood = True

        if not is_likelihood:
            result = False

        return result


