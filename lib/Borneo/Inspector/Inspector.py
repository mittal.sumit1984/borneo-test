import requests
import json
import logging
import time

LOGGER = logging.getLogger(__name__)

base_url = 'https://dlp.googleapis.com/v2/'


class Inspector:

    def __init__(self, project_id, api_key):
        """
        :param self:
        :param project_id: the Google Cloud project id to use as a parent resource.
        :param api_key:  api key to authenticate the api
        :return:
        """
        parent = f"projects/{project_id}"
        self.api_key = api_key
        self.url = base_url + parent + '/content:inspect'

    def headers(self):
        return {
            'Content-Type': 'application/json',
        }

    def inspect(
            self,
            content_string
    ):
        """
        :param content_string: The string to inspect.
        :return: response of the API
        """

        params = {'key': self.api_key}

        data = {
            "item": {
                "value": content_string
            }
        }

        LOGGER.info("DATA: " + json.dumps(data))
        LOGGER.info("URL: " + self.url)

        return requests.post(
            self.url,
            headers=self.headers(),
            params=params,
            data=json.dumps(data)
        )

# if __name__ == '__main__':
#     import os
#     ob = Inspector(project_id="qe-assignment",
#         api_key = 'AIzaSyBwdXHzGzCuPmlNzN2VQUl_g5ILsqBZW6g')
#     data = "email is email@123.123.123.123"
#
#     response = ob.inspect(data)
#     print(response.json())
