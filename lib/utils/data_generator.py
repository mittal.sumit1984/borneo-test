import random
import requests
import string
from faker import Faker

# Below data has generated with the help of Faker
VALID_SSN = [
    "137-87-2452",
    "294-48-7155",
    "465-58-4926",
    "091-96-8756",
    "434-63-2930",
    "167-86-0766",
    "851-06-6904",
    "351-42-0801",
    "504-98-6364",
    "647-36-8131",
    "673-30-6748",
    "606-22-6017",
    "445-49-9539",
    "128-93-9530",
    "235-60-0237",
    "343-70-0542",
    "623-31-7855",
    "821-04-2590",
    "240-30-6551",
    "439-95-0107"
]

INVALID_SSN = [
    "002-28-1852",
    "042-10-3580",
    "062-36-0749",
    "078-05-1120",
    "095-07-3645",
    "128-03-6045",
    "135-01-6629",
    "141-18-6941",
    "165-16-7999",
    "165-18-7999",
    "165-20-7999",
    "165-22-7999",
    "165-24-7999",
    "189-09-2294",
    "212-09-7694",
    "212-09-9999",
    "306-30-2348",
    "308-12-5070",
    "468-28-8779",
    "549-24-1889",
    "987-65-4320",
    "987-65-4321",
    "987-65-4322",
    "987-65-4323",
    "987-65-4324",
    "987-65-4325",
    "987-65-4326"
    "987-65-4327",
    "987-65-4328",
    "987-65-4329"]

VALID_EMAILS = [
    "email@example.com",
    "firstname.lastname@example.com",
    "email@subdomain.example.com",
    "firstname+lastname@example.com",
    '"email"@example.com"',
    "1234567890@example.com",
    "email@example-one.com",
    "_______@example.com",
    "email@example.name",
    "email@example.museum",
    "email@example.co.jp",
    "firstname-lastname@example.com",
    'あいうえお@example.com',
    "promero@yahoo.com",
    "tmoore@gmail.com",
    "haileychang@parker-gregory.net",
    "chavez@hotmail.com",
    "aaronjones@hotmail.com",
    "wanggregory@sandoval.com",
    "mcdonaldmanuel@jones.com",
    "tklein@yahoo.com",
    "william80@yahoo.com",
    "ulewis@hotmail.com",
]

INVALID_EMAILS = [
    'plainaddress',
    '#@%^%#$@#$@#.com',
    '@example.com',
    'Joe Smith <email@example.com>',
    'email.example.com',
    'email.@example.com',
    'email..email@example.com',
    'email@example',
    'email@-example.com',
    'email@111.222.333.44444',
    'email@example..com',
    'Abc..123@example.com'
]

VALID_CREDIT_CARDS = [
    "676123431592",
    "2296611279211174",
    "2252804372437778",
    "213135080137448",
    "3543554443046580",
    "378175176111544",
    "379725366749419",
    "30010543627056",
    "3523252371884700",
    "370897545008535",
    "675991140269",
    "5134962372581644",
    "377171525337717",
    "30007153248256",
    "30289990725510",
    "4453055859723605879",
    "180011439290379",
    "3595588539107895",
    "4823658762229559",
    "4488391242644"
]

INVALID_CREDITCARDS = [
    4758272817201586,
    4485159408815371,
    4556104726610403448,
    5362093834466373,
    5327202723609647,
    5291793824502683,
    345920443043131,
    378347986626223,
    374627533347181,
    6011994477600504,
    6011201807224488,
    6011421307234862556J,
    3544335436990961,
    3543833161976057,
    3541696038147964869,
    5530900710705467,
    5496176826161018,
    5431261383032345,
    30560041901510,
    30270119613126,
    30165910910947,
    36444515219176,
    36128148317727,
    36824454829970,
    5018218411827311,
    5020245408159073,
    5893574318083907,
    4913458788182523,
    4913197508605609,
    4913750516517628,
    6380205268464817,
    6393147804521129,
    6393080728097316
]

VALID_IPV4 = [
    "103.20.187.16",
    "18.150.186.153",
    "208.73.233.154",
    "183.187.109.122",
    "61.32.106.168",
    "9.208.140.76",
    "49.119.72.162",
    "36.181.186.23",
    "102.35.68.67",
    "39.166.91.201",
    "215.41.89.154",
    "80.169.40.193",
    "209.120.229.239",
    "182.64.238.33",
    "23.193.191.92",
    "122.115.186.122",
    "85.100.194.54",
    "71.57.156.143",
    "197.60.219.239",
    "30.2.43.252"
]

VALID_IBAN = [
    "GB14TJPB72410724879125",
    "GB35BRQN74899460859935",
    "GB39SOIW99037061697143",
    "GB55KZYY41702406927946",
    "GB95LTNQ36122730197674",
    "GB28GVVV85578177948779",
    "GB80CZTS48908681979598",
    "GB46FGNG35804461500444",
    "GB44VJTG31725150798337",
    "GB54YOJP08398344382910",
    "GB63FJHY38026541763894",
    "GB13ZLLQ40331323185672",
    "GB42IRUX25601730291706",
    "GB80QNNG57636893870915",
    "GB61OFNB48213450533192",
    "GB91DWJF03278753693975",
    "GB25GTQD41009263243357",
    "GB73HYZV83463328314389",
    "GB08XJEY71448119732380",
    "GB48YLWK52096018339959"
]

VALID_PHONE_NUMBERS = [
    "(666)270-2329",
    "(465)236-9142",
    "001-419-440-0107",
    "820.699.49020",
    "042.701.2627x851",
    "742-479-7651",
    "194-685-42417507",
    "599-783-2071977",
    "(814)400-0464845",
    "(256)036-5279"
    "995877736363"
]


def generate_fake_us_dl():
    """
    :return: fake driving license number
    """
    url = 'https://fauxid.com/tools/api-fake-drivers-license'
    headers = {
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    }

    data = {
        'stateSelect': 'All States'
    }

    response = requests.post(
        url,
        headers=headers,
        data=data)
    return response.json().get("licensenum")


def generate_fake_data(type_of_data, valid=True, count=5):
    """
    :param type_of_data:  type of data to be generated
                      i.e. name, ssn, email, creditcard, en-sentence, ip-address, drivers-license, bank-ibn
    :param valid: data should be valid or invalid(True or False)
    :param count: number of data points to be generated
    :return: list of the data set
    """

    values = []
    if not valid:
        faker = Faker('cz_CZ')
    elif valid:
        faker = Faker('en_US')
    if type_of_data == "name":
        for x in range(int(count) * 5):
            if valid:
                values.append(
                    random.choice(
                        [
                            faker.first_name(),
                            faker.last_name(),
                            faker.first_name_male(),
                            faker.first_name_female(),
                            faker.last_name_male(),
                            faker.last_name_female(),
                            faker.name()]))
            else:
                values.append(random.choice([faker.email(), faker.word(
                ) + " " + faker.word(), faker.currency_name(), faker.job()]))
            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "ssn":
        for x in range(int(count)):
            if valid:
                values += VALID_SSN
                # values.append(faker.ssn())
            else:
                values += INVALID_SSN
                values.append(random.choice([
                    "000-" + "-".join(faker.ssn().split("-")[1:]),
                    "666-" + "-".join(faker.ssn().split("-")[1:]),
                    "900-" + "-".join(faker.ssn().split("-")[1:]),
                    "-".join(faker.ssn().split("-")[0:2]) + "-000",
                ]))
            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "email":
        for x in range(int(count)):
            if valid:
                values += VALID_EMAILS
                # values.append(faker.email())
            else:
                values += INVALID_EMAILS
                values.append(random.choice([faker.name(), faker.word(
                ) + " " + faker.word(), faker.currency_name(), faker.job()]))
            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "creditcard":
        for x in range(int(count)):
            if valid:
                values += VALID_CREDIT_CARDS
                # values.append(faker.credit_card_number())
            else:
                values += INVALID_EMAILS

            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "phone-numbers":
        for x in range(int(count)):
            if valid:
                values += VALID_PHONE_NUMBERS
            else:
                values.append(random.choice([faker.name(), faker.word(
                ) + " " + faker.word(), faker.currency_name(), faker.job()]))

            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "ip-address":
        for x in range(int(count)):
            if valid:
                values += VALID_IPV4
                # values.append(faker.ipv4())
            else:
                values.append(random.choice([faker.name(), faker.word(
                ) + " " + faker.word(), faker.currency_name(), faker.job()]))

            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "drivers-license":
        for x in range(int(count)):
            if valid:
                values.append(generate_fake_us_dl())
            else:
                values.append(random.choice([faker.name(), faker.word(
                ) + " " + faker.word(), faker.currency_name(), faker.job()]))

            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))
    elif type_of_data == "bank-iban":
        for x in range(int(count)):
            if valid:
                values += VALID_IBAN
                # values.append(faker.iban())
            else:
                values.append(random.choice([faker.name(), faker.word(
                ) + " " + faker.word(), faker.currency_name(), faker.job()]))

            values = list(set(values))
            if len(list(set(values))) == count:
                return values
            elif len(values) > count:
                return random.sample(values, int(count))


def generate_sample_payloads(
        label_types,
        valid_data=True,
        dataset_per_label=1,
        pre_padding=15,
        post_padding=15):
    """
    :param valid_data: is data valid or not
    :param label_types: list of the type of labels
    :param dataset_per_label: number of data set per label
    :param pre_padding: prepend the size of this string. default - 15
    :param post_padding: append the size of this string. default - 15
    :return: payload
    """
    print("labels to generate payload for: " + str(label_types))
    payloads = {}
    payload_data = []
    final_payload = []
    for ltype in label_types:
        payload_data = generate_fake_data(ltype, valid_data, dataset_per_label)
        payloads[ltype] = payload_data

    # TODO: generate the variety of sample data
    for k, values in payloads.items():
        for v in values:
            # TODO - add the support for to customize the padding string from the user
            # pre_padding_string = ''.join(
            #     random.choice(string.ascii_uppercase + string.digits) for _ in range(pre_padding - 1))
            # post_padding_string = ''.join(
            #     random.choice(string.ascii_uppercase + string.digits) for _ in range(post_padding-1))
            #
            # final_payload.append("{} {} {}".format(pre_padding_string, v, post_padding_string))
            final_payload.append("Sample input is {}".format(v))

    return final_payload

# if __name__ == '__main__':
#     print(generate_fake_data("drivers-license", valid=False, count=5))
#     print(generate_sample_payloads(["name", "email", "ssn"], dataset_per_label=3))
